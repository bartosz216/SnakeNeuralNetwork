import pygame
import sys
from pygame.locals import *
from snake.apple import Apple
from snake.snake import Snake
from snake.map import Map


class App:
    def __init__(self, map_file, collector, controller):
        self._running = True
        self._display_surf = None
        self.size = self.weight, self.height = 600, 640
        self._map_file = map_file
        self._collector = collector
        self._controller = controller

    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode(self.size)
        pygame.display.set_caption('Snake')
        self._running = True

        # Font
        self._font = pygame.font.SysFont('Arial', 20)

        # Clock
        self._clock = pygame.time.Clock()

        self._map = Map(self._map_file)
        self._map.on_init()

        self._snake = Snake(self.size, self._controller)
        self._snake.on_init()

        self._apple = Apple()
        self._apple.on_init()
        self._apple.generate_new_pos(self._map)

    def on_event(self):
        for event in pygame.event.get(pygame.QUIT):
            self._running = False
        self._snake.on_event()

        pygame.event.clear()

    def on_loop(self):
        self._snake.on_loop()

        if(self._snake.hit_himself() or self._snake.hit_wall(self._map)):
            self.on_die()
            self._running = False
            return

        snake_xs, snake_ys = self._snake.get_head_pos()
        if collide(snake_xs, self._apple.applepos[0], snake_ys, self._apple.applepos[1], 20, 20, 20, 20):
            self._apple.generate_new_pos(self._map)
            self._snake.on_collision()

        self._collector.collect(self._snake, self._apple, self._map)

    def on_render(self):
        self._display_surf.fill((255, 255, 255))

        self._map.on_render(self._display_surf)
        self._snake.on_render(self._display_surf)
        self._apple.on_render(self._display_surf)

        t = self._font.render(str(self._snake.get_score()), True, (0, 0, 0))
        self._display_surf.blit(t, (10, 10))
        pygame.display.update()

    def on_cleanup(self):
        self._collector.save()
        pygame.quit()

    def on_execute(self):
        if self.on_init() == False:
            self._running = False
        while(self._running):
            self._clock.tick(10)
            self.on_event()
            self.on_loop()
            self.on_render()
        self.on_cleanup()

    def on_die(self):
        f = pygame.font.SysFont('Arial', 30)
        t = f.render('Your score was: ' +
                     str(self._snake.get_score()), True, (0, 0, 0))
        self._display_surf.blit(t, (10, 270))
        pygame.display.update()
        pygame.time.wait(2000)


def collide(x1, x2, y1, y2, w1, w2, h1, h2):
    if x1+w1 > x2 and x1 < x2+w2 and y1+h1 > y2 and y1 < y2+h2:
        return True
    else:
        return False
