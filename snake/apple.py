import random
import pygame

class Apple:
    def __init__(self):
        self._size = 20, 20

    def generate_new_pos(self, game_map):
        while True:
            new_x = random.randint(0, game_map.width-1)
            new_y = random.randint(0, game_map.height-1)
            if game_map.map[new_y][new_x] == False:
                self.applepos = (new_x*20, new_y*20 + 40)
                break
    
    def hit(self, pos):
        return collide(pos[0], self.applepos[0], pos[1], self.applepos[1], 20, 1, 20, 1)

    def on_init(self):
        self._appleimage = pygame.Surface(self._size)
        self._appleimage.fill((0, 255, 0))

    def on_loop(self):
        pass

    def on_render(self, display_surf):
        display_surf.blit(self._appleimage, self.applepos)

def collide(x1, x2, y1, y2, w1, w2, h1, h2):
    if x1+w1 > x2 and x1 < x2+w2 and y1+h1 > y2 and y1 < y2+h2:
        return True
    else:
        return False
