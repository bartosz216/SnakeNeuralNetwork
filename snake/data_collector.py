import csv
import itertools
import os


class DataCollector:
    def __init__(self):
        self._data = []
        self._file_path = 'data/data.csv'
   
    def clear_file(self):
        if os.path.exists('data'):
            open(self._file_path, 'w', newline='').close()

    def _get_data_for_dir(self, snake, apple, game_map, direction):
        start_x, start_y = snake.get_head_pos()
        start_x = int(start_x/20)
        start_y = int((start_y-40)/20)
        dist = 1
        hit_apple = False
        while True:
            x = (start_x + dist*direction[0]) % game_map.width
            y = (start_y + dist*direction[1]) % game_map.height
            if snake.hit((x*20, y*20+40)) or game_map.map[y][x]:
                break
            elif apple.hit((x*20, y*20+40)):
                hit_apple = True
                break
            dist += 1

        return dist, hit_apple

    def _classify_snake_direction(self, direction):
        directions = {0: [1, 0, 0, 0], 1: [0, 1, 0, 0],
                      2: [0, 0, 1, 0], 3: [0, 0, 0, 1]}
        return directions[direction]

    def collect(self, snake, apple, game_map):
        directions = [(0, -1), (1, -1), (1, 0), (1, 1),
                      (0, 1), (-1, 1), (-1, 0), (-1, -1)]
        data = [self._get_data_for_dir(
            snake, apple, game_map, direction) for direction in directions]
        data = list(itertools.chain(*data))
        data.extend(self._classify_snake_direction(snake.get_direction()))
        self._data.append(data)

    def save(self):
        if not os.path.exists('data'):
            os.makedirs('data')
        with open(self._file_path, 'a', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(self._data)
