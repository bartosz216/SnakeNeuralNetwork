from snake.app import App
from snake.data_collector import DataCollector
from snake.empty_data_collector import EmptyDataCollector
from snake.manual_controller import ManualController
from snake.random_controller import RandomController


def run_app(map_file = None, collect_data = False, controller_mode = 'manual', clear = False):
    if collect_data:
        collector = DataCollector()
    else:
        collector = EmptyDataCollector()

    controllers = { 'manual': ManualController(),
                    'random': RandomController() }

    if controller_mode in controllers:
        controller = controllers[controller_mode]
    else:
        controller = ManualController()

    if clear:
        collector.clear_file()

    app = App(map_file, collector, controller)
    app.on_execute()