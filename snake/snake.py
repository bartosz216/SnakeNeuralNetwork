import pygame


class Snake:
    def __init__(self, map_size, controller):
        self._map_size = map_size
        self._controller = controller

    def on_init(self):
        self._img = pygame.Surface((20, 20))
        self._img.fill((255, 0, 0))

        self._dirs = 0
        self._score = 0

        self._xs = [280, 280, 280, 280, 280]
        self._ys = [280, 260, 240, 220, 200]

    def on_event(self):
        self._dirs = self._controller.get_direction(self._dirs)

    def get_head_pos(self):
        return self._xs[0], self._ys[0]

    def get_score(self):
        return self._score

    def get_direction(self):
        return self._dirs

    def hit_himself(self):
        i = len(self._xs)-1
        while i >= 2:
            if collide(self._xs[0], self._xs[i], self._ys[0], self._ys[i], 20, 20, 20, 20):
                return True
            i -= 1
        return False

    def hit(self, pos):
        i = len(self._xs)-1
        while i >= 0:
            if collide(pos[0], self._xs[i], pos[1], self._ys[i], 20, 1, 20, 1):
                return True
            i -= 1
        return False

    def hit_wall(self, game_map):
        x = int(self._xs[0]/20)
        y = int((self._ys[0]-40)/20)
        return game_map.map[y][x]

    def on_loop(self):
        i = len(self._xs)-1
        while i >= 1:
            self._xs[i] = self._xs[i-1]
            self._ys[i] = self._ys[i-1]
            i -= 1

        dirs = {
            0: lambda x, y: (x,      y + 20),
            1: lambda x, y: (x + 20, y),
            2: lambda x, y: (x,      y - 20),
            3: lambda x, y: (x - 20, y),
        }

        if self._dirs in dirs:
            self._xs[0], self._ys[0] = dirs[self._dirs](
                self._xs[0], self._ys[0])

        if self._xs[0] >= self._map_size[0]:
            self._xs[0] = 0
        elif self._xs[0] < 0:
            self._xs[0] = self._map_size[0]-20

        if self._ys[0] >= self._map_size[1]:
            self._ys[0] = 40
        elif self._ys[0] < 40:
            self._ys[0] = self._map_size[1]-20

    def on_render(self, display_surf):
        for i in range(0, len(self._xs)):
            display_surf.blit(self._img, (self._xs[i], self._ys[i]))

    def on_collision(self):
        self._score += 1
        self._xs.append(700)
        self._ys.append(700)


def collide(x1, x2, y1, y2, w1, w2, h1, h2):
    if x1+w1 > x2 and x1 < x2+w2 and y1+h1 > y2 and y1 < y2+h2:
        return True
    else:
        return False
