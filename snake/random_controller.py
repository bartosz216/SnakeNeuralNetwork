import random

class RandomController:
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

    def get_direction(self, current_direction):
        rand = random.randint(0,3)
        if current_direction == 2 and rand != 0: return rand
        if current_direction == 0 and rand != 2: return rand
        if current_direction == 3 and rand != 1: return rand
        if current_direction == 1 and rand != 3: return rand
        return current_direction