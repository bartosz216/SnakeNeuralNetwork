import pygame


class Map:
    def __init__(self, file_path = None):
        self.map = []
        self.width = 0
        self.height = 0
        self._file_path = file_path

    def on_init(self):
        self._empty_field = pygame.Surface((20, 20))
        self._empty_field.fill((255, 255, 255))

        self._occupied_field = pygame.Surface((20, 20))
        self._occupied_field.fill((0, 0, 0))

        if self._file_path != None:
            with open(self._file_path, 'r') as f:
                size = f.readline()
                splitted_size = size.split()
                self.width = int(splitted_size[0])
                self.height = int(splitted_size[1])
                self.map = [[field == '1' for field in line.split()] for line in f.readlines()]
        else:
            self.width, self.height = (30, 30)
            self.map = [[False for _ in range(self.height)] for _ in range(self.width)]

    def on_render(self, display_surf):
        for y in range(self.height):
            for x in range(self.width):
                cords = (x*20, y*20 + 40)
                if self.map[y][x] == True:
                    display_surf.blit(self._occupied_field, cords)
                else:
                    display_surf.blit(self._empty_field, cords)
