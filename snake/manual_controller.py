
import pygame
from pygame.locals import *

class ManualController:
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

    def get_direction(self, current_direction):
        for event in pygame.event.get(KEYDOWN):
            keys = {
                K_UP: lambda dir: 2 if dir != 0 else dir,
                K_DOWN: lambda dir: 0 if dir != 2 else dir,
                K_LEFT: lambda dir: 3 if dir != 1 else dir,
                K_RIGHT: lambda dir: 1 if dir != 3 else dir,
            }

            if event.key in keys:
                return keys[event.key](current_direction)
        return current_direction