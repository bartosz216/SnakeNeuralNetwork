# SnakeNeuralNetwork
usage: snake.py [-h] [-m MAP_FILE] [-c]

optional arguments:
* -h, --help - show this help message and exit
* -m MAP_FILE, --map MAP_FILE - Path to map file
* -d, --collect_data - Turn on data collection
* --clear - Clear collected data. Can be used when data collection is turned on.
* -c {manual, random}, --controller {manual, random} - Choose controlling mode
