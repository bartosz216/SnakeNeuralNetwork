from snake import run_app
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--map', action='store', dest='map_file',
                    help='Path to map file', default=None)
    parser.add_argument('-d', '--collect_data', action='store_true', dest='collect_data',
                    help='Turn on data collection')
    parser.add_argument('--clear', action='store_true', dest='clear',
                    help='Clear collected data. Can be used when data collection is turned on.')
    parser.add_argument('-c', '--controller', action='store', dest='controller',
                    help='Choose controlling mode', default='manual', choices=['manual', 'random'])

    args = parser.parse_args()
    run_app(args.map_file, args.collect_data, args.controller, args.clear)
